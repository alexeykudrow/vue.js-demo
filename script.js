function supportsLocalStorage() {
    try {
        return 'localStorage' in window && window['localStorage'] !== null;
    } catch (e) {
        return false;
    }
}

(function() {
    var categories = {
        1: 'Зеленые',
        2: 'Синие',
        3: 'Красные',
    }

    var items_all = {
        1: {
            title: 'Lorem ipsum',
            image: 'http://via.placeholder.com/550x350/488c31/fff?text=Lorem ipsum',
            category_id: 1,
            price: 388000,
        },
        2: {
            title: 'Donec convallis',
            image: 'http://via.placeholder.com/550x350/121cde/fff?text=Donec convallis',
            category_id: 2,
            price: 282000,
        },
        3: {
            title: 'Nullam ac augue',
            image: 'http://via.placeholder.com/550x350/48ad25/fff?text=Nullam ac augue',
            category_id: 1,
            price: 414000,
        },
        4: {
            title: 'Pellentesque ornare',
            image: 'http://via.placeholder.com/550x350/d33153/fff?text=Pellentesque ornare',
            category_id: 3,
            price: 432000,
        },
        5: {
            title: 'Nunc feugiat turpis',
            image: 'http://via.placeholder.com/550x350/3176d3/fff?text=Nunc feugiat turpis',
            category_id: 2,
            price: 343000,
        },
        6: {
            title: 'Sed aliquet orci.',
            image: 'http://via.placeholder.com/550x350/aaf62c/fff?text=Sed aliquet orci.',
            category_id: 1,
            price: 372000,
        },
        7: {
            title: 'Vivamus luctus',
            image: 'http://via.placeholder.com/550x350/868cfe/fff?text=Vivamus luctus',
            category_id: 2,
            price: 48000,
        },
        8: {
            title: 'Mauris sit amet',
            image: 'http://via.placeholder.com/550x350/98ef7b/fff?text=Mauris sit amet',
            category_id: 1,
            price: 437000,
        },
        9: {
            title: 'Aliquam mollis.',
            image: 'http://via.placeholder.com/550x350/ed6e88/fff?text=Aliquam mollis.',
            category_id: 3,
            price: 35000,
        },
        10: {
            title: 'Ut ut massa',
            image: 'http://via.placeholder.com/550x350/a1fe86/fff?text=Ut ut massa',
            category_id: 1,
            price: 366000,
        },
        11: {
            title: 'Aliquam malesuada',
            image: 'http://via.placeholder.com/550x350/a81f3b/fff?text=Aliquam malesuada',
            category_id: 3,
            price: 191000,
        },
        12: {
            title: 'Donec imperdiet',
            image: 'http://via.placeholder.com/550x350/b8b9ff/fff?text=Donec imperdiet',
            category_id: 2,
            price: 233000,
        },
        13: {
            title: 'Morbi sagittis',
            image: 'http://via.placeholder.com/550x350/f62c56/fff?text=Morbi sagittis',
            category_id: 3,
            price: 470000,
        },
        14: {
            title: 'Proin cursus',
            image: 'http://via.placeholder.com/550x350/b8d6ff/fff?text=Proin cursus',
            category_id: 2,
            price: 130000,
        },
    };

    // В локальном хранилище хранятся только id объектов
    var items_favorite = [];

    if (supportsLocalStorage() && localStorage.key('items_favorite')) {
        items_favorite = JSON.parse(localStorage.getItem('items_favorite'));
    }

    var app = new Vue({
        el: '#app',
        data: {
            show_favorites: false,
            filter_category_id: 0,
            categories: categories,
            items_all: items_all,
            items_favorite: items_favorite,
        },

        computed: {
            // Все избранные объекты и полный список возвращаются через общюю переменную
            items: function() {
                if (this.show_favorites) {
                    // Отображение избранного - возвращается список объектов по их id
                    var items = {};

                    for(var i = 0; i < this.items_favorite.length; i++){
                        items[this.items_favorite[i]] = items_all[this.items_favorite[i]];
                    }

                    return items;
                } else {
                    return this.items_all;
                }
            },
        },

        methods: {
            countItems: function() {
                // Подсчет количества выводимых объектов с учетом фильтра
                var count = 0;

                for (var id in this.items) {
                    if (!this.items.hasOwnProperty(id)) continue;

                    if (
                        this.filter_category_id == 0
                        || this.items[id].category_id == this.filter_category_id
                    ) {
                        count++;
                    }
                }

                return count;
            },

            isInFavorites: function(item_id) {
                item_id = parseInt(item_id);
                return this.items_favorite.indexOf(item_id) > -1;
            },

            toggleFavorite: function(item_id) {
                item_id = parseInt(item_id);
                var index = this.items_favorite.indexOf(item_id);

                if (index > -1) {
                    this.items_favorite.splice(index, 1);
                } else {
                    this.items_favorite.push(item_id);
                }

                if (supportsLocalStorage()) {
                    localStorage.setItem('items_favorite', JSON.stringify(this.items_favorite));
                }
            },

            toggleView: function() {
                this.show_favorites = !this.show_favorites;
                this.filter_category_id = 0;
            },
        },
    });
}());